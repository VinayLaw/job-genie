import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { FormsModule } from '@angular/forms';
import { HttpClient } from 'selenium-webdriver/http';
import { InformationService } from './services/information/information.service';
import { ApiService } from './services/genericApiMethods/api.service';
import { HttpClientModule } from '@angular/common/http';
import { JobsComponent } from './components/jobs/jobs.component';
import { JobListComponent } from './components/jobs/components/job-list/job-list.component';
import { DetailComponent } from './components/jobs/components/detail/detail.component';
import { FilterJobsPipe } from './pipes/filterJobs/filter-jobs.pipe';
import { CallbackComponent } from './components/callback/callback.component';
import { DialogModule } from 'primeng/dialog';
import { AuthService } from './services/auth/auth.service';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    JobsComponent,
    JobListComponent,
    DetailComponent,
    FilterJobsPipe,
    CallbackComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AutoCompleteModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    DialogModule
  ],
  providers: [AuthService, InformationService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
