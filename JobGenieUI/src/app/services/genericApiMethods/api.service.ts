import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  accessToken: any;
  proxy: string;
  headers: HttpHeaders = new HttpHeaders();
  spinnerActivator: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    this.proxy = 'https://cors-anywhere.herokuapp.com/';
  }

  // get request form the front-end to back-end
  // header goes to the request section of the api where we can even put Authorization,
  // as api end points are given by you so have not put it, just have put 'content-type'
  get(apiUrl) {
    return this.httpClient.get(this.proxy + apiUrl, { headers: this.headers });
  }

}
