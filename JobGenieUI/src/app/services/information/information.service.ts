import { Injectable } from '@angular/core';
import { ApiService } from '../genericApiMethods/api.service';
import { MasterData } from 'src/app/models/masterdata';
import { BehaviorSubject, Observable } from 'rxjs';
import { CommonData } from 'src/app/models/commonData';
import { filter } from 'minimatch';

@Injectable({
  providedIn: 'root'
})
export class InformationService {

  // variables
  masterData: BehaviorSubject<MasterData[]> = new BehaviorSubject<MasterData[]>([]);
  requestedData: MasterData[] = [];
  // end

  constructor(private apiService: ApiService) {

  }

  getMasterData() {
    this.apiService.get('https://nut-case.s3.amazonaws.com/jobs.json')
      .subscribe((data: any[]) => {
        const index = 'data';
        this.masterData.next(data[index]);
      });
  }

  getRequestedData(filteredData: CommonData[]) {
    let mainData: MasterData[] = [];
    mainData = this.masterData.getValue();
    mainData.filter((job) => {
      const index = filteredData.findIndex(x => {
        if (x.Name === job.skills || x.Name === job.title || x.Name === job.companyname) {
          return true;
        }
      });
      if (index !== -1) {
        return true;
      }
    }).forEach((item) => {
      if (this.requestedData.findIndex(x => x.title.replace(/\s/g, '').toLowerCase()
      === item.title.replace(/\s/g, '').toLowerCase()) === -1) {
        this.requestedData.push(item);
      }
    });
  }

  getUniqueList(masterData: MasterData[], result: CommonData[], property: string) {
    result = [];
    masterData.map((data, index) => {
      return {
        Name: data[property],
        Id: index,
        Selected: false
      };
    }).forEach((item) => {
      if (result.findIndex(x => x.Name.replace(/\s/g, '').toLowerCase() === item.Name.replace(/\s/g, '').toLowerCase()) === -1) {
        result.push(item);
      }
    });
    return result;
  }
}
