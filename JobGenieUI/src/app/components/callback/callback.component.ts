import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit {

  constructor(private auth: AuthService) {
  }
  display: boolean;
  message: string;
  ngOnInit() {
    this.auth.handleAuthCallback();
    this.display = true;
    this.message = 'please wait building the page';
  }

}
