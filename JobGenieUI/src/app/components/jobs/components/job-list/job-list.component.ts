import { Component, OnInit } from '@angular/core';
import { MasterData } from 'src/app/models/masterdata';
import { InformationService } from 'src/app/services/information/information.service';
import { CommonData } from 'src/app/models/commonData';
import { Router } from '@angular/router';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {

  // variables
  requestedData: MasterData[] = [];
  locations: CommonData[] = [];
  experience: CommonData[] = [];
  // end
  constructor(private infoService: InformationService, private router: Router) { }

  ngOnInit() {
    this.requestedData = this.infoService.requestedData;
    if (this.requestedData === null || this.requestedData.length === 0) {
      this.goBackToMainPage();
    }
    this.getFilterData();
  }

  getFilterData() {
    const masterdata = this.infoService.requestedData;
    this.locations = this.infoService.getUniqueList(masterdata, this.locations, 'location');
    this.experience = this.infoService.getUniqueList(masterdata, this.experience, 'experience');
  }

  goBackToMainPage() {
    this.infoService.requestedData = [];
    this.router.navigate(['home']);
  }

  jobDetail(id) {
    this.router.navigate(['/jobs/' + id]);
  }
}
