import { Component, OnInit } from '@angular/core';
import { InformationService } from 'src/app/services/information/information.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MasterData } from 'src/app/models/masterdata';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  // variables
  jobId: string;
  job: MasterData = new MasterData();
  // end
  constructor(
    private infoService: InformationService,
    private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.jobId = params.id;
      if (this.infoService.requestedData === null || this.infoService.requestedData.length === 0) {
        this.back();
      }
      this.getJobDetail();
    });
  }

  getJobDetail() {
    const index = this.infoService.requestedData.findIndex(x => x._id === this.jobId);
    if (index !== -1) {
      this.job = this.infoService.requestedData[index];
    }
  }

  apply(url) {
    window.location.href = url;
  }

  back() {
    this.router.navigate(['jobs']);
  }
}
