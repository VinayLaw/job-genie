import { Component, OnInit, OnDestroy } from '@angular/core';
import { InformationService } from 'src/app/services/information/information.service';
import { MasterData } from 'src/app/models/masterdata';
import { Subscription } from 'rxjs';
import { CommonData } from 'src/app/models/commonData';
import { RouterModule, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  // variables
  masterData: MasterData[] = [];
  suggestionData: CommonData[] = [];
  allFilterData: CommonData[] = [];
  seletedData: CommonData[] = [];
  validator: boolean;
  // end

  // subscriptions
  masterDataSubscription: Subscription;
  // end
  constructor(private infoService: InformationService, private router: Router) {
    this.validator = true;
  }

  ngOnInit() {
    this.checkForMasterData();
  }

  processMasterData(masterData: MasterData[]) {
    let skills: CommonData[] = [];
    let titles: CommonData[] = [];
    let companies: CommonData[] = [];
    skills = this.infoService.getUniqueList(masterData, skills, 'skills');
    titles = this.infoService.getUniqueList(masterData, titles, 'title');
    companies = this.infoService.getUniqueList(masterData, companies, 'companyname');
    this.allFilterData = skills.concat(titles.concat(companies));
  }

  checkForMasterData() {
    this.masterDataSubscription = this.infoService.masterData.subscribe((data: MasterData[]) => {
      if (data.length !== 0) {
        this.processMasterData(data);
      }
    });
  }

  filteredData(event) {
    const query = event.query;
    this.suggestionData = this.filterData(query);
  }

  filterData(query: string) {
    return this.allFilterData.filter((item) => item.Name.toLowerCase().indexOf(query) !== -1);
  }

  getRequestedData() {
    if (this.seletedData.length > 0) {
      this.validator = true;
      this.infoService.getRequestedData(this.seletedData);
      if (this.infoService.requestedData.length > 0) {
        this.router.navigate(['jobs']);
      } else {
        this.validator = false;
      }
    } else {
      this.validator = false;
    }
  }

  ngOnDestroy() {
    this.masterDataSubscription.unsubscribe();
  }
}
