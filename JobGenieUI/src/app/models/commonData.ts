export class CommonData {
    Name: string;
    Id: number;
    Selected: boolean;

    constructor() {
        this.Selected = false;
    }
}
