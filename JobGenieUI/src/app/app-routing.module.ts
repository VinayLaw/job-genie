import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { JobListComponent } from './components/jobs/components/job-list/job-list.component';
import { DetailComponent } from './components/jobs/components/detail/detail.component';
import { CallbackComponent } from './components/callback/callback.component';
import { AuthGuard } from './guards/authguard/auth.guard';


const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  {
    path: 'jobs', component: JobsComponent, canActivate: [AuthGuard],
    children: [
      { path: '', component: JobListComponent },
      { path: ':id', component: DetailComponent }
    ]
  },
  { path: 'callback', component: CallbackComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
