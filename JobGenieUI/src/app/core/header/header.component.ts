import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user: any;
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.getUserDetails();
  }

  getUserDetails() {
    this.auth.userProfile$.subscribe((user) => {
      this.user = user;
    });
  }
 logout() {
   this.auth.logout();
 }
}
