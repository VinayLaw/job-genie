import { Pipe, PipeTransform } from '@angular/core';
import { MasterData } from 'src/app/models/masterdata';
import { CommonData } from 'src/app/models/commonData';

@Pipe({
  name: 'filterJobs',
  pure: false
})
export class FilterJobsPipe implements PipeTransform {
  queriedData: MasterData[] = [];
  transform(value: MasterData[], locations: CommonData[], experience: CommonData[]): any {
    const selectedLocations = locations.filter(x => {
      if (x.Selected === true) {
        return true;
      }
    });

    const selectedExperience = experience.filter(x => {
      if (x.Selected === true) {
        return true;
      }
    });

    if (value.length === 0 && selectedExperience.length === 0
      && selectedLocations.length === 0) {
      return value;
    }

    if (selectedExperience.length === 0
      && selectedLocations.length === 0) {
      return value;
    }

    const queriedDataByLocation = value.filter(x => {
      if (selectedLocations.find(y => y.Name.replace(/\s/g, '').toLowerCase()
      === x.location.replace(/\s/g, '').toLowerCase()) !== undefined) {
        return true;
      }
    });

    const queriedDataByExperience = value.filter(x => {
      if (selectedExperience.find(y => y.Name.replace(/\s/g, '').toLowerCase()
      === x.experience.replace(/\s/g, '').toLowerCase()) !== undefined) {
        return true;
      }
    });

    if (queriedDataByExperience.length > 0 && queriedDataByLocation.length > 0) {
      this.queriedData = queriedDataByLocation.filter((x) => {
        const index = queriedDataByExperience.findIndex(y => x.title === y.title);
        if (index !== -1) {
          return true;
        }
      });
    } else {
      this.queriedData = queriedDataByLocation.concat(queriedDataByExperience);
    }

    return this.queriedData;
  }

}

