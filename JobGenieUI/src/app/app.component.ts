import { Component, OnInit } from '@angular/core';
import { InformationService } from './services/information/information.service';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'JobGenieUI';
  constructor(private infoService: InformationService, private auth: AuthService) {

  }
  ngOnInit() {
    this.auth.localAuthSetup();
    this.infoService.getMasterData();
  }
}
