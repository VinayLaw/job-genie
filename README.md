Hi this is Sai Vinay writing the details about the project.

Lets start with the Architecture of the project.

	Will concentrate in the app folder as there the main code reside
	1. So the app folder is divied into sub-folders as
		-> components
			This folder has different components or pages of the website like home, job-list etc. 
		-> services
			1. Services folder has all the services classes which help in communication between different components, have shared data
				and also call the http methods like 'get, put, post etc'.
			2. I have made one of the class as generic api class by the name api.service, its main use is to keep the mathods like get, post put etc,
			   	which can be used by all the other service classes easily. 
			3. Have made use of BehaviourSubjects for communication between different components through the service. 
		-> pipes
			This folder contains pipe which is used by job-list component for filtering of the searched result. 
		-> core
			This folder contains core components like header, footer etc, as the project was not that vast could not come up with more core components. 
		-> models
			this folder keeps all the models, like in this project i have a MasterData model and CommonModel. 
		-> guards
			this folder keeps the authGuards which is being used while routing to check if the user is authenticated to see the page or not. 

	2. Features that i tried to cover. 
	
	    	-> Have tried to show 2 levels of routing i.e. directly calling the component from the path and making 
			a parent child component relationship and calling the components. 
	   
	 	-> Made use of observables, behavious subjects and have showed how to do sharing of data form 1 component to another. 
	   
	    	-> Have done sso using auth-0 authentication and have showed how to use auth guard. 
	
		-> Have used the feature of pipes for the filter purpose in the job-list page. 

		-> Have tried to perform re-usability of code and have minimised the calling of the api to it max and concentrated of data sharing rather 
			than trying and getting the fresh data again and again. 
	
		-> Have used Bootstarp and ng-prime components. 

	Now lets see how the application works.
		
		-> First you login through the auth-0 sso. 
		
		-> You enter the home-page. 
		
		-> There you can search the kind of job you need based on skills, company and job title.
		
		-> After you have searched and if its a valid one, you are taken to job-list page then.
		
		-> In this page you can further filter the data based in location and experience.

		-> If you want to change the search criteria you have to go back to home page by clicking 'Make a wish again' button.
	
		-> We can see the details of the job by clicking the detail button of the job card and there we can apply for the job as it re-directs to the 
			apply job link available in the master json of the api provided. 


Note 
	-> Its not any excuse but  I could have showed more features but did not get much time as I had to take time from my office hours and do this. 
	-> I would like to get reply for this if the application came out as expected or not. 
	
